var express = require("express");
var bodyParser = require("body-parser");
var webdriverio = require('webdriverio');

var options = {
    desiredCapabilities: {
        browserName: 'chrome'
    }
};

var app = express();
app.use(bodyParser.json());

app.post('/', function (req, res) {
  webdriverio
      .remote(options)
      .init()
      .url('http://www.amazon.com')
      .click('#nav-link-accountList')
      .setValue('#ap_email', req.body.email)
      .setValue('#ap_password', req.body.password)
      .click('#signInSubmit')
      .getUrl().then(function isLoggedIn(url){
         if(url !== 'https://www.amazon.com/?ref_=nav_ya_signin&')
           console.log("wrong credentials");
      })
      .end()
      res.send("done");
})

app.listen(9000, function () {
  console.log('Server is listening on port 9000');
})
